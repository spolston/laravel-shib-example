<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (isset($_SERVER["uid"])){
        return "Hello: " . $_SERVER["uid"];
      } else {
        return redirect("/shib-login");
      }
});

Route::get('/shib-login', function () {
    return redirect("/");
});