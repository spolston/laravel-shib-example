<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShibController extends Controller
{
    /*
    *
    */
    
    public function sendHome()
    {
        return redirect("/");
    }

    public function checkWebID()
    {
        if (isset($_SERVER["uid"])){
            return $this->printName($_SERVER["uid"]);
        } else {
            return redirect("/shib-login");
        }
    }

    public function printName( $webid )
    {
        return "Hello: " . $webid;
    }
}
