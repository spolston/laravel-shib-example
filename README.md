 # Laravel Shibboleth Examples - OleMiss

## About

 This repository is designed to be a set of examples for using [Laravel](https://laravel.com/) along with [Shibboleth](https://www.shibboleth.net/) at the [University of Mississippi](http://olemiss.edu).

## Getting Started

This repository assumes that you already have the following:
 * a Shibboleth Service Provider (SP) set up and funtioning with the OleMiss IdP
 * Apache
 * PHP 7
 * Composer

Using Composer, set up a starting [Laravel Project](https://laravel.com/docs/5.7/installation) and if necessary, use [this guide](https://laravel-news.com/subfolder-install) to set it up in a subfolder.

## Examples

### Example 1

This example is heavily influenced by the Slim Example I did. It works, but it's not exactly what I consider to be the "Laravel Way", and would struggle with fuller app designs.

### Example 2

This example is more in line with the "Laravel Way", utilizing [Controllers](https://laravel.com/docs/5.7/controllers) and object-oriented design.

The Controller file can be created in two ways: creating a file with the same name as the controller you're creating or with the command ```php artisan make:controller ShibController```. 